# Geofence Setel

This application detects user's device is located inside of a geofence area.

## Getting Started

Clone project from bitbucket. It should be able to build locally on the simulator without any issues.

In this application, I define a struct AccessPoint stands for the definition Geofence Area. It should include the name, note, identifier, coordinate, and radius.
To use the application on the simulator, you should enable the location service for the simulator (Features -> Location -> Select a mode)
I suggest you should select mode "Freeway Drive" or "City Bicycle Ride" for better observation.

To test the application, you can add a new AccessPoint ahead of the user's moving direction by touch to the "+" bar button. The radius should be greater than 200 for better detection.

## Built with

* [Swift 5](https://developer.apple.com/swift/) - The programming language used
* MVVM - Design Pattern
* No external libraries were used in this project
