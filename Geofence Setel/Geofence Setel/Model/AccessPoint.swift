//
//  AccessPoint.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation
import MapKit

final class AccessPoint: NSObject, Codable, MKAnnotation {
    let identifier: String
    let name: String
    let note: String
    let coordinate: CLLocationCoordinate2D
    let radius: CLLocationDistance
    
    var title: String? {
        if name.isEmpty {
            return "No name"
        }
        return name
    }
    
    var subtitle: String? {
        return "Radius: \(radius)m - \(note)"
    }
    
    enum CodingKeys: String, CodingKey {
        case identifier, name, note, radius, latitude, longitude
    }
    
    init(identifier: String, name: String, note: String, coordinate: CLLocationCoordinate2D, radius: CLLocationDistance) {
        self.identifier = identifier
        self.name = name
        self.note = note
        self.coordinate = coordinate
        self.radius = radius
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try values.decode(String.self, forKey: .identifier)
        name = try values.decode(String.self, forKey: .name)
        note = try values.decode(String.self, forKey: .note)
        radius = try values.decode(Double.self, forKey: .radius)
        let latitude = try values.decode(Double.self, forKey: .latitude)
        let longitude = try values.decode(Double.self, forKey: .longitude)
        coordinate = CLLocationCoordinate2DMake(latitude, longitude)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(name, forKey: .name)
        try container.encode(note, forKey: .note)
        try container.encode(radius, forKey: .radius)
        try container.encode(coordinate.latitude, forKey: .latitude)
        try container.encode(coordinate.longitude, forKey: .longitude)
    }
}

extension AccessPoint {
    var region: CLCircularRegion {
        let region = CLCircularRegion(center: coordinate,
                                      radius: radius,
                                      identifier: identifier)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        return region
    }
}
