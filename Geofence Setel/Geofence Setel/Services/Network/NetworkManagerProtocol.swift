//
//  NetworkManagerProtocol.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 07/12/2020.
//

import Foundation

protocol NetworkManagerProtocol {
    var connectedToWifi: Bool { get }
    var connectionOnChange: ((Bool) -> Void)? { get set }
    func start()
    func stop()
}
