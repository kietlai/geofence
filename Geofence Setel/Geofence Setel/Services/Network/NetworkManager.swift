//
//  NetworkManager.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 07/12/2020.
//

import Network

final class NetworkManager {
    private let queue = DispatchQueue(label: "WifiMonitor")
    private let monitor = NWPathMonitor(requiredInterfaceType: .wifi)
    private var pathStatus: NWPath.Status = .unsatisfied
    
    var connectionOnChange: ((Bool) -> Void)?
    
    init() {
        configureMonitor()
    }
}

// MARK: - Private

extension NetworkManager {
    private func configureMonitor() {
        monitor.pathUpdateHandler = { [weak self] path in
            debugPrint("⛑ pathUpdateHandler connected \(path)")
            self?.pathStatus = path.status
            self?.connectionOnChange?(path.status == .satisfied)
        }
    }
}

// MARK: - NetworkManagerProtocol

extension NetworkManager: NetworkManagerProtocol {
    var connectedToWifi: Bool { return pathStatus == .satisfied }
    
    func start() {
        monitor.start(queue: queue)
    }
    
    func stop() {
        monitor.cancel()
    }
}
