//
//  LocationRepository.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation

final class LocationRepository: NSObject {
    private lazy var locationManager = CLLocationManager()
    var delegate: LocationRepositoryDelegate?
    
    var onReceiveEvent: ((_ identifier: String, _ event: GeofenceEventType) -> Void)?
    var onError: ((GeoError) -> Void)?
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
}

// MARK: - Privates

extension LocationRepository {
    func handleEvent(for region: CLRegion, event: GeofenceEventType) {
        debugPrint("⛑ handleEvent \(event) for \(region)")
        onReceiveEvent?(region.identifier, event)
    }
}

// MARK: - LocationRepositoryProtocol

extension LocationRepository: LocationRepositoryProtocol {
    var maximumRegionMonitoringDistance: Double {
        return locationManager.maximumRegionMonitoringDistance
    }
    
    var locationServiceStatus: CLAuthorizationStatus {
        return locationManager.authorizationStatus
    }
    
    func configure(with delegate: LocationRepositoryDelegate) {
        self.delegate = delegate
        locationManager.requestAlwaysAuthorization()
    }
    
    func startMonitoringCurrentLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopMonitoringCurrentLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func startMonitoring(accessPoint: AccessPoint) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            onError?(.notSupport)
            return
        }
        switch locationManager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startMonitoring(for: accessPoint.region)
        default:
            onError?(.permission)
            return
        }
    }
    
    func stopMonitoring(accessPoint: AccessPoint) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion,
                  circularRegion.identifier == accessPoint.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension LocationRepository: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch locationManager.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            delegate?.didChangeAuthorization(status: status)
        default:
            onError?(.permission)
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastPosition = locations.last else { return }
        delegate?.didUpdateLocation(lastPosition)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        onError?(.failedMonitoringIdentifier(region?.identifier))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        onError?(.failedMonitoringWithError(error))
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        handleEvent(for: region, event: .onEnter)
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        handleEvent(for: region, event: .onExit)
    }
    
    /*
    /// Another way to monitor the enter/exit region, this method trigger more frequently but it may cause the duplicated events on multiple regions
     func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
         switch state {
         case .inside:
             handleEvent(for: region, event: .onEnter)
         case .outside, .unknown:
             handleEvent(for: region, event: .onExit)
         }
     }
    */
}
