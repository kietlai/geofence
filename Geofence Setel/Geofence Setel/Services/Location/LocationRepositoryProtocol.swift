//
//  LocationRepositoryProtocol.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation

protocol LocationRepositoryProtocol {
    var maximumRegionMonitoringDistance: Double { get }
    var locationServiceStatus: CLAuthorizationStatus { get }
    var onReceiveEvent: ((_ identifier: String, _ event: GeofenceEventType) -> Void)? { get set }
    var onError: ((GeoError) -> Void)? { get set }
    func configure(with delegate: LocationRepositoryDelegate)
    func startMonitoringCurrentLocation()
    func stopMonitoringCurrentLocation()
    func startMonitoring(accessPoint: AccessPoint)
    func stopMonitoring(accessPoint: AccessPoint)
}

protocol LocationRepositoryDelegate {
    func didChangeAuthorization(status: CLAuthorizationStatus)
    func didUpdateLocation(_ location: CLLocation)
}

enum GeofenceEventType {
    case onEnter
    case onExit
}

enum GeoError {
    case notSupport
    case permission
    case failedMonitoringIdentifier(String?)
    case failedMonitoringWithError(Error)
}
