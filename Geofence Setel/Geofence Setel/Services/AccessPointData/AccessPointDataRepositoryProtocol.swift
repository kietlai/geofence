//
//  AccessPointDataRepositoryProtocol.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

protocol AccessPointDataRepositoryProtocol {
    func load() -> [AccessPoint]
    func saveAll()
    func save(accessPoint: AccessPoint)
    func deleteAccessPoint(with identifier: String) -> AccessPoint?
    func getAccessPoint(by identifier: String) -> AccessPoint?
}
