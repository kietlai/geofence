//
//  AccessPointDataRepository.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import Foundation

final class AccessPointDataRepository {
    private let AccessPointDataKey = "AccessPointDataKey"
    private var accessPoints: [AccessPoint] = []
}

// MARK: - AccessPointDataRepositoryProtocol

extension AccessPointDataRepository: AccessPointDataRepositoryProtocol {
    func load() -> [AccessPoint] {
        guard let savedData = UserDefaults.standard.data(forKey: AccessPointDataKey) else { return accessPoints }
        accessPoints.removeAll()
        let decoder = JSONDecoder()
        if let savedGeotifications = try? decoder.decode(Array.self, from: savedData) as [AccessPoint] {
            accessPoints = savedGeotifications
        }
        return accessPoints
    }
    
    func saveAll() {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(accessPoints) {
            UserDefaults.standard.set(data, forKey: AccessPointDataKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    func save(accessPoint: AccessPoint) {
        if !accessPoints.contains(accessPoint) {
            accessPoints.append(accessPoint)
        }
    }
    
    func deleteAccessPoint(with identifier: String) -> AccessPoint? {
        if let index = accessPoints.lastIndex(where: { $0.identifier == identifier }) {
            return accessPoints.remove(at: index)
        }
        return nil
    }
    
    func getAccessPoint(by identifier: String) -> AccessPoint? {
        return accessPoints.first(where: { $0.identifier == identifier })
    }
}
