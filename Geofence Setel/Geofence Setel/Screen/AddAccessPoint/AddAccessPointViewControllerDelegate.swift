//
//  AddAccessPointViewControllerDelegate.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation

protocol AddAccessPointViewControllerDelegate {
    func saveAccessPoint(controller: AddAccessPointViewController,
                         identifier: String,
                         name: String,
                         description: String,
                         coordinate: CLLocationCoordinate2D,
                         radius: Double)
}
