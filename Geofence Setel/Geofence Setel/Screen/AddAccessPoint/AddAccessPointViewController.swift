//
//  AddAccessPointViewController.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import UIKit
import MapKit

final class AddAccessPointViewController: UITableViewController {
    @IBOutlet weak var accessPointName: UITextField!
    @IBOutlet weak var accessPointDescription: UITextField!
    @IBOutlet weak var radius: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    var startLocation: CLLocationCoordinate2D?
    var delegate: AddAccessPointViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapView()
    }
    
    @IBAction func save(_ sender: Any) {
        let name = accessPointName.text ?? ""
        let description = accessPointDescription.text ?? ""
        let coordinate = mapView.centerCoordinate
        let radiusValue = Double(radius.text!) ?? 0
        let identifier = name + "~~" + description
        delegate?.saveAccessPoint(controller: self,
                                  identifier: identifier,
                                  name: name,
                                  description: description,
                                  coordinate: coordinate,
                                  radius: radiusValue)
    }
}

// MARK: - Private

extension AddAccessPointViewController {
    private func setupMapView() {
        tableView.estimatedRowHeight = 500
        tableView.rowHeight = UITableView.automaticDimension
        if let location = startLocation {
            let region = MKCoordinateRegion(center: location,
                                            span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
            mapView.setRegion(region, animated: true)
        }
    }
}
