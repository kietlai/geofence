//
//  MapViewModel.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation

final class MapViewModel {
    // Dependencies
    lazy var locationRepository: LocationRepositoryProtocol = LocationRepository()
    lazy var accessPointDataRepository: AccessPointDataRepositoryProtocol = AccessPointDataRepository()
    lazy var networkManager: NetworkManagerProtocol = NetworkManager()
    
    // Private attributes
    private var notifications: [String] = []
    private var wifiConnection: Bool = false
    private var currentAccessPointState: GeofenceEventType = .onExit
    private var animatingNotification: Bool = false
    
    // Handlers
    var accessPointsDidLoad: (([AccessPoint]) -> Void)?
    var authorizationStatusHandler: ((CLAuthorizationStatus) -> Void)?
    var currentLocationOnChange: ((CLLocationCoordinate2D) -> Void)?
    var needDisplayAccessPoint: ((AccessPoint) -> Void)?
    var needDismissAccessPoint: ((AccessPoint) -> Void)?
    
    // Notification
    var currentAccessPoint: AccessPoint?
    var currentStatusOnChange: ((GeofenceAreaStatus, AccessPoint?) -> Void)?
    var needDisplayNotification: ((String) -> Void)?
}

// MARK: - Privates

extension MapViewModel {
    private func bindings() {
        networkManager.connectionOnChange = { [weak self] connected in
            self?.wifiConnection = connected
            self?.handleAccessPointData()
        }
        
        locationRepository.onReceiveEvent = { [weak self] (identifier, event) in
            if let accessPoint = self?.accessPointDataRepository.getAccessPoint(by: identifier) {
                self?.currentAccessPoint = accessPoint
                self?.currentAccessPointState = event
                self?.handleAccessPointData()
            }
        }
        
        locationRepository.onError = { [weak self] error in
            self?.handleError(error)
        }
    }
    
    private func handleAccessPointData() {
        guard let accessPoint = currentAccessPoint else { return }
        let status: GeofenceAreaStatus
        let message: String
        switch currentAccessPointState {
        case .onEnter:
            message = "On Enter \(accessPoint.identifier)"
            status = .inside
        case .onExit:
            message = "On Exit \(accessPoint.identifier)"
            status = wifiConnection ? .outsideButStillConnectWifi : .outside
        }
        showNotification(message)
        currentStatusOnChange?(status, accessPoint)
    }
    
    private func handleError(_ error: GeoError) {
        let message: String
        switch error {
        case .notSupport:
            message = "Geofencing is not supported on this device!"
        case .permission:
            message = "Permission denied!"
        case .failedMonitoringIdentifier(let identifier):
            message = "Monitoring failed for region with identifier: \(identifier ?? "Unknowned")"
        case .failedMonitoringWithError(let error):
            message = "Location Manager failed with the following error: \(error.localizedDescription)"
        }
        
        showNotification(message)
        currentStatusOnChange?(.outside, nil)
    }
}

// MARK: - MapViewModelProtocol

extension MapViewModel: MapViewModelProtocol {
    func startServices() {
        bindings()
        networkManager.start()
        accessPointsDidLoad?(accessPointDataRepository.load())
        locationRepository.configure(with: self)
    }
    
    func saveAllAccessPoints() {
        accessPointDataRepository.saveAll()
        showNotification("Saved all Access Points.")
    }
    
    func saveAccessPoint(identifier: String,
                         name: String,
                         description: String,
                         coordinate: CLLocationCoordinate2D,
                         radius: Double) {
        let bestRadius = max(min(radius, locationRepository.maximumRegionMonitoringDistance), 200)
        let accessPoint = AccessPoint(identifier: identifier,
                                      name: name,
                                      note: description,
                                      coordinate: coordinate,
                                      radius: bestRadius)
        accessPointDataRepository.save(accessPoint: accessPoint)
        locationRepository.startMonitoring(accessPoint: accessPoint)
        needDisplayAccessPoint?(accessPoint)
    }
    
    func accessPoint(for identifier: String) -> AccessPoint? {
        return accessPointDataRepository.getAccessPoint(by: identifier)
    }
    
    func removeAccessPoint(for identifier: String) {
        guard let accessPoint = accessPointDataRepository.deleteAccessPoint(with: identifier) else { return }
        locationRepository.stopMonitoring(accessPoint: accessPoint)
        needDismissAccessPoint?(accessPoint)
    }
    
    func showNotification(_ notification: String) {
        if !animatingNotification {
            animatingNotification = true
            needDisplayNotification?(notification)
        } else {
            notifications.insert(notification, at: 0)
        }
    }
    
    func nextNotification() {
        animatingNotification = false
        guard !notifications.isEmpty else { return }
        showNotification(notifications.removeFirst())
    }
}

// MARK: - LocationRepositoryDelegate

extension MapViewModel: LocationRepositoryDelegate {
    func didChangeAuthorization(status: CLAuthorizationStatus) {
        authorizationStatusHandler?(status)
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationRepository.startMonitoringCurrentLocation()
        }
    }
    
    func didUpdateLocation(_ location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                            longitude: location.coordinate.longitude)
        currentLocationOnChange?(center)
        locationRepository.stopMonitoringCurrentLocation()
    }
}
