//
//  MapViewController.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import UIKit
import MapKit

final class MapViewController: UIViewController {
    var viewModel: MapViewModelProtocol = MapViewModel()
    
    @IBOutlet weak var notificationHolderView: UIView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindings()
    }
    
    @IBAction func save(_ sender: Any) {
        viewModel.saveAllAccessPoints()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let addAccessPointVC = segue.destination as? AddAccessPointViewController else {
            return
        }
        addAccessPointVC.startLocation = mapView.userLocation.coordinate
        addAccessPointVC.delegate = self
    }
}

// MARK: - Privates

extension MapViewController {
    private func bindings() {
        viewModel.authorizationStatusHandler = { [weak self] status in
            self?.mapView?.showsUserLocation = (status == .authorizedAlways || status == .authorizedWhenInUse)
        }
        
        viewModel.accessPointsDidLoad = { [weak self] accessPoints in
            for accessPoint in accessPoints {
                self?.displayAccessPoint(accessPoint)
            }
        }
        
        viewModel.currentLocationOnChange = { [weak self] location in
            let region = MKCoordinateRegion(center: location,
                                            span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self?.mapView?.setRegion(region, animated: true)
        }
        
        viewModel.needDisplayAccessPoint = { [weak self] accessPoint in
            self?.displayAccessPoint(accessPoint)
        }
        
        viewModel.needDismissAccessPoint = { [weak self] accessPoint in
            self?.removeAnnotation(for: accessPoint)
        }
        
        viewModel.currentStatusOnChange = { [weak self] status, accessPoint in
            self?.updateGeofenceStatus(status, accessPoint: accessPoint)
        }
        
        viewModel.needDisplayNotification = { [weak self] notification in
            self?.showNotification(notification)
        }
        
        viewModel.startServices()
    }
    
    private func showNotification(_ notification: String) {
        notificationLabel.text = notification
        UIView.animate(withDuration: 0.3) {
            self.notificationHolderView.alpha = 1
        } completion: { _ in
            UIView.animate(withDuration: 0.3, delay: 0.6, options: .curveLinear) {
                self.notificationHolderView.alpha = 0
            } completion: { _ in
                self.viewModel.nextNotification()
            }
        }
    }
    
    private func displayAccessPoint(_ accessPoint: AccessPoint) {
        setupAnnotation(for: accessPoint)
        addRadiusOverlay(for: accessPoint)
    }
    
    private func setupAnnotation(for accessPoint: AccessPoint) {
        mapView?.addAnnotation(accessPoint)
    }
    
    private func addRadiusOverlay(for accessPoint: AccessPoint) {
        mapView?.addOverlay(MKCircle(center: accessPoint.coordinate, radius: accessPoint.radius))
    }
    
    private func removeAnnotation(for accessPoint: AccessPoint) {
        mapView?.removeAnnotation(accessPoint)
        removeRadiusOverlay(for: accessPoint)
    }
    
    private func removeRadiusOverlay(for accessPoint: AccessPoint) {
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == accessPoint.coordinate.latitude
                && coord.longitude == accessPoint.coordinate.longitude
                && circleOverlay.radius == accessPoint.radius {
                mapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
    func updateGeofenceStatus(_ status: GeofenceAreaStatus, accessPoint: AccessPoint?) {
        switch status {
        case .inside:
            statusLabel.textColor = .blue
            statusLabel.text = "inside \n Access point: " + (accessPoint?.name ?? "")
        case .outside:
            statusLabel.textColor = .red
            statusLabel.text = "outside"
        case .outsideButStillConnectWifi:
            statusLabel.textColor = .orange
            statusLabel.text = "outside \n still connect to Wifi"
        }
    }
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let accessPoint = annotation as? AccessPoint else { return nil }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: accessPoint.identifier) as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: accessPoint, reuseIdentifier: accessPoint.identifier)
            annotationView?.canShowCallout = true
            let removeButton = UIButton(type: .custom)
            removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
            removeButton.setImage(UIImage(named: "cross"), for: .normal)
            annotationView?.leftCalloutAccessoryView = removeButton
        } else {
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .systemBlue
            circleRenderer.fillColor = UIColor.systemBlue.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let identifier = view.reuseIdentifier {
            viewModel.removeAccessPoint(for: identifier)
        }
    }
}

// MARK: - AddAccessPointViewControllerDelegate

extension MapViewController: AddAccessPointViewControllerDelegate {
    func saveAccessPoint(controller: AddAccessPointViewController,
                         identifier: String,
                         name: String,
                         description: String,
                         coordinate: CLLocationCoordinate2D ,
                         radius: Double) {
        viewModel.saveAccessPoint(identifier: identifier,
                                  name: name,
                                  description: description,
                                  coordinate: coordinate,
                                  radius: radius)
        navigationController?.popViewController(animated: true)
    }
}
