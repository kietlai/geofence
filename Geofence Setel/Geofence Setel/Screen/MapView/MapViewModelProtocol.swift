//
//  MapViewModelProtocol.swift
//  Geofence Setel
//
//  Created by Kiet Lai on 06/12/2020.
//

import CoreLocation

protocol MapViewModelProtocol {
    var authorizationStatusHandler: ((CLAuthorizationStatus) -> Void)? { get set }
    var currentLocationOnChange: ((CLLocationCoordinate2D) -> Void)? { get set }
    var accessPointsDidLoad: (([AccessPoint]) -> Void)? { get set }
    var needDisplayAccessPoint: ((AccessPoint) -> Void)? { get set }
    var needDismissAccessPoint: ((AccessPoint) -> Void)? { get set }
    func startServices()
    func saveAllAccessPoints()
    func saveAccessPoint(identifier: String,
                         name: String,
                         description: String,
                         coordinate: CLLocationCoordinate2D,
                         radius: Double)
    func accessPoint(for identifier: String) -> AccessPoint?
    func removeAccessPoint(for identifier: String)
    
    // Notification
    var currentStatusOnChange: ((GeofenceAreaStatus, AccessPoint?) -> Void)? { get set }
    var needDisplayNotification: ((String) -> Void)? { get set }
    func showNotification(_ notification: String)
    func nextNotification()
}

enum GeofenceAreaStatus {
    case inside
    case outside
    case outsideButStillConnectWifi
}
