//
//  MapViewModelTests.swift
//  Geofence SetelTests
//
//  Created by Kiet Lai on 09/12/2020.
//

import XCTest
import CoreLocation
@testable import Geofence_Setel

class MapViewModelTests: XCTestCase {
    var sut: MapViewModel!

    func testStartServices() {
        // Given:
        let accessPoint = AccessPoint(identifier: "mock-identifier", name: "mock-name", note: "mock-note",
                                      coordinate: CLLocationCoordinate2D(latitude: 1.2, longitude: 0.4), radius: 123.53)
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        
        let mockNetworkManager = MockNetworkManagerProtocol()
        sut.networkManager = mockNetworkManager
        
        let mockAccessPointDataRepository = MockAccessPointDataRepositoryProtocol()
        mockAccessPointDataRepository.loadedAccessPoints = [accessPoint]
        sut.accessPointDataRepository = mockAccessPointDataRepository
        
        let expected = expectation(description: #function)
        
        // When:
        sut.accessPointsDidLoad = { accessPoints in
            XCTAssertEqual(accessPoints, mockAccessPointDataRepository.loadedAccessPoints)
            expected.fulfill()
        }
        sut.startServices()
        
        // Then:
        XCTAssertTrue(mockLocationRepository.configureGetCalled)
        XCTAssertTrue(mockNetworkManager.startGetCalled)
        XCTAssertTrue(mockAccessPointDataRepository.loadGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testSaveAllAccessPoints() {
        // Given:
        sut = MapViewModel()
        let mockAccessPointDataRepository = MockAccessPointDataRepositoryProtocol()
        sut.accessPointDataRepository = mockAccessPointDataRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.needDisplayNotification = { notification in
            XCTAssertEqual(notification, "Saved all Access Points.")
            expected.fulfill()
        }
        sut.saveAllAccessPoints()
        
        // Then:
        XCTAssertTrue(mockAccessPointDataRepository.saveAllGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testSaveAccessPoint() {
        // Given:
        let identifier = "mock-identifier"
        let name = "mock-name"
        let description = "mock-description"
        let coordinate = CLLocationCoordinate2D(latitude: 1.2, longitude: 0.4)
        let radius = 123.45
        
        sut = MapViewModel()
        
        let mockAccessPointDataRepository = MockAccessPointDataRepositoryProtocol()
        sut.accessPointDataRepository = mockAccessPointDataRepository
        
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.needDisplayAccessPoint = { accessPoint in
            XCTAssertEqual(accessPoint, mockAccessPointDataRepository.savedAccessPoint)
            expected.fulfill()
        }
        sut.saveAccessPoint(identifier: identifier, name: name, description: description,
                            coordinate: coordinate, radius: radius)
        
        // Then:
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.identifier, identifier)
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.name, name)
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.note, description)
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.coordinate.latitude, coordinate.latitude)
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.coordinate.longitude, coordinate.longitude)
        XCTAssertEqual(mockAccessPointDataRepository.savedAccessPoint?.radius, radius)
        XCTAssertEqual(mockLocationRepository.startMonitoringGetCalledWithAccessPoint, mockAccessPointDataRepository.savedAccessPoint)
        waitForExpectations(timeout: 0.1)
    }
    
    func testAccessPointForIdentifier() {
        // Given:
        let identifier = "mock-identifier"
        let name = "mock-name"
        let note = "mock-note"
        let coordinate = CLLocationCoordinate2D(latitude: 1.2, longitude: 0.4)
        let radius = 123.45
        let accessPoint = AccessPoint(identifier: identifier, name: name, note: note, coordinate: coordinate, radius: radius)
        
        sut = MapViewModel()
        let mockAccessPointDataRepository = MockAccessPointDataRepositoryProtocol()
        mockAccessPointDataRepository.mockGetAccessPoint = accessPoint
        sut.accessPointDataRepository = mockAccessPointDataRepository
        
        // When:
        let expectedAccessPoint = sut.accessPoint(for: identifier)
        
        // Then:
        XCTAssertEqual(mockAccessPointDataRepository.getAccessPointGetCalledWithIdentifier, identifier)
        XCTAssertEqual(expectedAccessPoint, accessPoint)
    }
    
    func testRemoveAccessPoint() {
        // Given:
        let identifier = "mock-identifier"
        let name = "mock-name"
        let note = "mock-note"
        let coordinate = CLLocationCoordinate2D(latitude: 1.2, longitude: 0.4)
        let radius = 123.45
        let accessPoint = AccessPoint(identifier: identifier, name: name, note: note, coordinate: coordinate, radius: radius)
        
        sut = MapViewModel()
        
        let mockAccessPointDataRepository = MockAccessPointDataRepositoryProtocol()
        mockAccessPointDataRepository.mockDeletedAccessPoint = accessPoint
        sut.accessPointDataRepository = mockAccessPointDataRepository
        
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.needDismissAccessPoint = { value in
            XCTAssertEqual(value, accessPoint)
            expected.fulfill()
        }
        sut.removeAccessPoint(for: identifier)
        
        // Then:
        XCTAssertEqual(mockAccessPointDataRepository.mockDeletedAccessPoint, accessPoint)
        XCTAssertEqual(mockAccessPointDataRepository.deleteAccessPointGetCalledWithIdentifier, identifier)
        XCTAssertEqual(mockLocationRepository.stopMonitoringGetCalledWithAccessPoint, accessPoint)
        waitForExpectations(timeout: 0.1)
    }
    
    func testShowNotification() {
        // Given:
        let notification = "mock-notification"
        sut = MapViewModel()
        let expected = expectation(description: #function)
        
        // When:
        sut.needDisplayNotification = { value in
            XCTAssertEqual(value, notification)
            expected.fulfill()
        }
        sut.showNotification(notification)
        
        // Then:
        waitForExpectations(timeout: 0.1)
    }
    
    func testShowNotificationShouldPutToStackIfAnimating() {
        // Given:
        let notification1 = "mock-notification1"
        let notification2 = "mock-notification2"
        let notification3 = "mock-notification3"
        
        sut = MapViewModel()
        let expected = expectation(description: #function)
        expected.expectedFulfillmentCount = 1
        
        // When:
        sut.needDisplayNotification = { value in
            XCTAssertEqual(value, notification1)
            expected.fulfill()
        }
        sut.showNotification(notification1)
        sut.showNotification(notification2)
        sut.showNotification(notification3)
        
        // Then:
        waitForExpectations(timeout: 0.1)
    }
    
    func testNextNotificationEmptyShouldNotCallNeedDisplayNotification() {
        // Given:
        sut = MapViewModel()
        let expected = expectation(description: #function)
        expected.isInverted = true
        
        // When:
        sut.needDisplayNotification = { _ in
            expected.fulfill()
        }
        sut.nextNotification()
        
        // Then:
        waitForExpectations(timeout: 0.1)
    }
    
    func testNextNotificationEmptyShouldCallNeedDisplaySecondNotification() {
        // Given:
        let notification1 = "mock-notification1"
        let notification2 = "mock-notification2"
        
        sut = MapViewModel()
        let expected = expectation(description: #function)
        expected.expectedFulfillmentCount = 2
        
        var callCount = 0
        
        // When:
        sut.needDisplayNotification = { value in
            if callCount == 0 {
                XCTAssertEqual(value, notification1)
            } else {
                XCTAssertEqual(value, notification2)
            }
            callCount += 1
            expected.fulfill()
        }
        sut.showNotification(notification1)
        sut.nextNotification()
        sut.showNotification(notification2)
        
        // Then:
        waitForExpectations(timeout: 0.1)
    }
    
    func testDidChangeAuthorizationWithAuthorizedAlways() {
        // Given:
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.authorizationStatusHandler = { status in
            XCTAssertEqual(status, CLAuthorizationStatus.authorizedAlways)
            expected.fulfill()
        }
        sut.didChangeAuthorization(status: .authorizedAlways)
        
        // Then:
        XCTAssertTrue(mockLocationRepository.startMonitoringCurrentLocationGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testDidChangeAuthorizationWithAuthorizedWhenInUse() {
        // Given:
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.authorizationStatusHandler = { status in
            XCTAssertEqual(status, CLAuthorizationStatus.authorizedWhenInUse)
            expected.fulfill()
        }
        sut.didChangeAuthorization(status: .authorizedWhenInUse)
        
        // Then:
        XCTAssertTrue(mockLocationRepository.startMonitoringCurrentLocationGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testDidChangeAuthorizationWithDenied() {
        // Given:
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.authorizationStatusHandler = { status in
            XCTAssertEqual(status, CLAuthorizationStatus.denied)
            expected.fulfill()
        }
        sut.didChangeAuthorization(status: .denied)
        
        // Then:
        XCTAssertFalse(mockLocationRepository.startMonitoringCurrentLocationGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testDidChangeAuthorizationWithNotDetermined() {
        // Given:
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.authorizationStatusHandler = { status in
            XCTAssertEqual(status, CLAuthorizationStatus.notDetermined)
            expected.fulfill()
        }
        sut.didChangeAuthorization(status: .notDetermined)
        
        // Then:
        XCTAssertFalse(mockLocationRepository.startMonitoringCurrentLocationGetCalled)
        waitForExpectations(timeout: 0.1)
    }
    
    func testDidChangeAuthorizationWithRestricted() {
        // Given:
        sut = MapViewModel()
        let mockLocationRepository = MockLocationRepositoryProtocol()
        sut.locationRepository = mockLocationRepository
        let expected = expectation(description: #function)
        
        // When:
        sut.authorizationStatusHandler = { status in
            XCTAssertEqual(status, CLAuthorizationStatus.restricted)
            expected.fulfill()
        }
        sut.didChangeAuthorization(status: .restricted)
        
        // Then:
        XCTAssertFalse(mockLocationRepository.startMonitoringCurrentLocationGetCalled)
        waitForExpectations(timeout: 0.1)
    }
}
