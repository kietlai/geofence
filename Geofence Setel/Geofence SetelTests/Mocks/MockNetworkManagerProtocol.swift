//
//  MockNetworkManagerProtocol.swift
//  Geofence SetelTests
//
//  Created by Kiet Lai on 09/12/2020.
//

@testable import Geofence_Setel

class MockNetworkManagerProtocol: NetworkManagerProtocol {
    var connectedToWifi: Bool = false
    
    var connectionOnChange: ((Bool) -> Void)? = nil
    
    var startGetCalled = false
    func start() {
        startGetCalled = true
    }
    
    var stopGetCalled = false
    func stop() {
        stopGetCalled = true
    }
    
    
}
