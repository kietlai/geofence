//
//  MockLocationRepositoryProtocol.swift
//  Geofence SetelTests
//
//  Created by Kiet Lai on 09/12/2020.
//

import CoreLocation
@testable import Geofence_Setel

class MockLocationRepositoryProtocol: LocationRepositoryProtocol {
    var mockMaximumRegionMonitoringDistance: Double = 123.455
    var maximumRegionMonitoringDistance: Double {
        get {
            return mockMaximumRegionMonitoringDistance
        }
    }
    
    var mockLocationServiceStatus: CLAuthorizationStatus = .notDetermined
    var locationServiceStatus: CLAuthorizationStatus {
        get {
            return mockLocationServiceStatus
        }
    }
    
    var onReceiveEvent: ((String, GeofenceEventType) -> Void)? = nil
    
    var configureGetCalled = false
    func configure(with delegate: LocationRepositoryDelegate) {
        configureGetCalled = true
    }
    
    var startMonitoringCurrentLocationGetCalled: Bool = false
    func startMonitoringCurrentLocation() {
        startMonitoringCurrentLocationGetCalled = true
    }
    
    var stopMonitoringCurrentLocationGetCalled = false
    func stopMonitoringCurrentLocation() {
        stopMonitoringCurrentLocationGetCalled = true
    }
    
    var startMonitoringGetCalledWithAccessPoint: AccessPoint? = nil
    func startMonitoring(accessPoint: AccessPoint) {
        startMonitoringGetCalledWithAccessPoint = accessPoint
    }
    
    var stopMonitoringGetCalledWithAccessPoint: AccessPoint? = nil
    func stopMonitoring(accessPoint: AccessPoint) {
        stopMonitoringGetCalledWithAccessPoint = accessPoint
    }
    
    
}
