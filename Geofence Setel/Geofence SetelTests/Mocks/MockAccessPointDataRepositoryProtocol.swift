//
//  MockAccessPointDataRepositoryProtocol.swift
//  Geofence SetelTests
//
//  Created by Kiet Lai on 09/12/2020.
//

@testable import Geofence_Setel

class MockAccessPointDataRepositoryProtocol: AccessPointDataRepositoryProtocol {
    var loadGetCalled = false
    var loadedAccessPoints: [AccessPoint] = []
    func load() -> [AccessPoint] {
        loadGetCalled = true
        return loadedAccessPoints
    }
    
    var saveAllGetCalled = false
    func saveAll() {
        saveAllGetCalled = true
    }
    
    var savedAccessPoint: AccessPoint? = nil
    func save(accessPoint: AccessPoint) {
        savedAccessPoint = accessPoint
    }
    
    var deleteAccessPointGetCalledWithIdentifier: String? = nil
    var mockDeletedAccessPoint: AccessPoint? = nil
    func deleteAccessPoint(with identifier: String) -> AccessPoint? {
        deleteAccessPointGetCalledWithIdentifier = identifier
        return mockDeletedAccessPoint
    }
    
    var getAccessPointGetCalledWithIdentifier: String? = nil
    var mockGetAccessPoint: AccessPoint? = nil
    func getAccessPoint(by identifier: String) -> AccessPoint? {
        getAccessPointGetCalledWithIdentifier = identifier
        return mockGetAccessPoint
    }
}
