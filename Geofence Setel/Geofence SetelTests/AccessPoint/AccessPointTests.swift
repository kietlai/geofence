//
//  AccessPointTests.swift
//  Geofence SetelTests
//
//  Created by Kiet Lai on 09/12/2020.
//

import XCTest
import CoreLocation
@testable import Geofence_Setel

class AccessPointTests: XCTestCase {
    var sut: AccessPoint!
    
    func testRegion() {
        // Given:
        let identifier = "mock-identifier"
        let name = "mock-name"
        let note = "mock-note"
        let latitude: Double = 1.222
        let longitude: Double = 0.0345
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let radius: Double = 123.455
        
        // When:
        let sut = AccessPoint(identifier: identifier, name: name, note: note, coordinate: coordinate, radius: radius)
        let region = sut.region
        
        // Then:
        XCTAssertEqual(region.radius, radius)
        XCTAssertEqual(region.center.latitude, latitude)
        XCTAssertEqual(region.center.longitude, longitude)
        XCTAssertEqual(region.identifier, identifier)
        XCTAssertTrue(region.notifyOnEntry)
        XCTAssertTrue(region.notifyOnExit)
    }

}
